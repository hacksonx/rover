package com.next45.rovers;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class AppTest extends TestCase{


    public void testClassGivesResponse() {
        String sizeOfGrid = "88";
        String startingPoints = "12 E";
        String commands = "MMLMRMMRRMML";
        Rover rover = new Rover(sizeOfGrid, startingPoints, commands);
        Assert.assertTrue(rover.toString().length() > 0);
    }

    public void testChangeDirectionFromEastToNorth() {
        String sizeOfGrid = "11";
        String startingPoints = "11 E";
        String commands = "L";
        Rover rover = new Rover(sizeOfGrid, startingPoints, commands);
        Assert.assertTrue(rover.toString().equals("1 1 N"));
    }

    public void testChangeDirectionFromEastToSouth() {
        String sizeOfGrid = "11";
        String startingPoints = "11 E";
        String commands = "R";
        Rover rover = new Rover(sizeOfGrid, startingPoints, commands);
        Assert.assertTrue(rover.toString().equals("1 1 S"));
    }

    public void testMoveOneBlock() {
        String sizeOfGrid = "22";
        String startingPoints = "11 E";
        String commands = "M";
        Rover rover = new Rover(sizeOfGrid, startingPoints, commands);
        Assert.assertTrue(rover.toString().equals("1 2 E"));
    }

    
    public void testAPreventedMoveDueToGoingOutOfBounds() {
        String sizeOfGrid = "22";
        String startingPoints = "11 W";
        String commands = "M";
        Rover rover = new Rover(sizeOfGrid, startingPoints, commands);
        Assert.assertTrue(rover.toString().equals("1 1 W"));
    }
}
