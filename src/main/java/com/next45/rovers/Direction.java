package com.next45.rovers;

public enum Direction {
    E,
    W,
    N,
    S
}