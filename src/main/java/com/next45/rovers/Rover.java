package com.next45.rovers;

public class Rover {
    private Direction facing;
    private int numberOfCols;
    private int numberOfRows;
    private int row;
    private int col;

    public Rover(String sizeOfGrid, String startingPoints, String commands) {
        this.numberOfRows = Character.getNumericValue(sizeOfGrid.charAt(0));
        this.numberOfCols = Character.getNumericValue(sizeOfGrid.charAt(1));
        this.row = Character.getNumericValue(startingPoints.charAt(0));
        this.col = Character.getNumericValue(startingPoints.charAt(1));
        setFacing(startingPoints.charAt(3));
        processCommands(commands);
    }

    public String toString() {
        return String.format("%s %s %s", this.row, this.col, this.facing.toString());
    }

    private void processCommands(String commands) {
        for (char command : commands.toCharArray()) {
            performACommand(command);
        }
    }

    private void setFacing(char facing) {
        switch (facing) {
        case 'E':
            this.facing = Direction.E;
            break;
        case 'W':
            this.facing = Direction.W;
            break;
        case 'N':
            this.facing = Direction.N;
            break;
        case 'S':
            this.facing = Direction.S;
            break;
        default:
            break;
        }
    }

    public void performACommand(char userCommand) {
        switch (userCommand) {
        case 'M':
            switch (facing) {
            case E:
                if ((this.col + 1) <= this.numberOfCols) {
                    this.col += 1;
                }
                break;
            case W:
                if ((this.col - 1) > 0) {
                    this.col -= 1;
                }
                break;
            case S:
                if ((this.row + 1) <= this.numberOfRows) {
                    this.row += 1;
                }
                break;
            case N:
                if ((this.row - 1) > 0) {
                    this.row -= 1;
                }
                break;
            }
            break;
        case 'R':
            switch (facing) {
            case E:
                this.facing = Direction.S;
                break;
            case W:
                this.facing = Direction.N;
                break;
            case S:
                this.facing = Direction.W;
                break;
            case N:
                this.facing = Direction.E;
                break;
            }
            break;
        case 'L':
            switch (this.facing) {
            case E:
                this.facing = Direction.N;
                break;
            case W:
                this.facing = Direction.S;
                break;
            case S:
                this.facing = Direction.E;
                break;
            case N:
                this.facing = Direction.W;
                break;
            }
            break;
        }
    }

}
