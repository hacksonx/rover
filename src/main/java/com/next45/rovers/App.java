package com.next45.rovers;

public class App 
{
    public static void main( String[] args )
    {
        Rover rover = new Rover(args[0], args[1], args[2]);
        System.out.println(rover.toString());
    }
}
