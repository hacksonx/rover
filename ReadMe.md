The Rover

Notes
1. Couldn't get the specified output from the specified example input.

Assumptions
1. Cannot move outside bounds. Therefore can't go into negatives

How to run
1. Install maven and java
2. Run mvn clean install
3. Modify tests for other inputs
4. Run mvn test

Design
1. Designed to spec by creating a class that initiallises to input by taking inputs directly from the constructor and running commands immediatly.
2. Output returned by overriding the object toString java method.
3. Extracted compass directions to enums for readablity
